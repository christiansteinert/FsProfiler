open System
open System.IO

open Newtonsoft.Json

open Suave
open Suave.Operators
open Suave.Filters
open Suave.Successful

let inline (<*>) x y = Path.Combine(x, y)

let header = """
        <div class="row">
            <div class="col-md-12">
                <ul class="tree">
                    <!--<p class="well" style="height:135px;">
                        <strong>Fancy Text</strong>

                        <br /> <code>temp\tasks.log</code>
                    </p>-->
"""
let footer = """
                </ul>
            </div>
        </div>
"""

printfn "%s" Environment.CurrentDirectory
let html = File.ReadAllText (Environment.CurrentDirectory <*> "Content" <*> "index2.html")
let rec formatTask (task : FsProfiler.Task) = 
    if task.SubTasks |> List.isEmpty then
        sprintf "<li>%s - %dms</li>" task.Name task.DurationInMilliseconds
    else
        sprintf "<li><a href=\"#\">%s - %dms</a><ul>%s</ul></li>" task.Name task.DurationInMilliseconds (task.SubTasks |> List.map formatTask |> List.reduce (+))

let routes =
    choose
        [
            path "/analyze" >=> request
                (fun req -> 
                    req.files.[0].tempFilePath 
                    |> File.ReadAllLines 
                    |> Array.map (JsonConvert.DeserializeObject<FsProfiler.Task> >> formatTask)
                    |> Array.fold (fun acc c -> acc + header + c + footer) String.Empty
                    |> fun c -> html.Replace("{{REPLACEME}}", c)
                    |> OK)
            path "/" >=> Redirection.MOVED_PERMANENTLY "/index.html"
            request (fun req ->
                req.path.Substring(1, req.path.Length - 1) |>
                (<*>) (Environment.CurrentDirectory <*> "Content")
                |> Files.file)
        ]

startWebServer defaultConfig routes