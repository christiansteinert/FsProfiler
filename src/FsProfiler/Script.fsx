#r @"..\..\packages\Newtonsoft.Json\lib\net45\Newtonsoft.Json.dll"
#load "FsProfiler.fs"
#load "Listeners.fs"
#load "Codecs.fs"
#r "System.Xml.Linq"

open System

open FsProfiler
open FsProfiler.Listeners
open FsProfiler.Codecs

// Prepare some compuations

let wait (ms : int) = 
    System.Threading.Thread.Sleep ms

let DP () =
    let qwe (fp : DisposingProfiler) = 
        use fp2 = fp.StartSubtask "DP Level 1"
        wait 5
        use __ = fp2.StartSubtask "DP Level 2"
        wait 4
    use fp = new DisposingProfiler "DP Level 0"
    qwe fp    
    let qwe' =
        use __ = fp.StartSubtask "DP Level 1-2"
        wait 8
    let __ = qwe'
    wait 20

let WP () =
    let impl () = 
        wait 8
    
    WrappingProfiler.Profile "Test" impl

let TP () =
    let tp = TransientProfiler "TP 1"
    wait 2 // Prepare stuff
    wait 10
    let tp2 = 
        let tp = tp.GetSubProfiler "TP 2" // Get and start a sub profiler
        tp
    wait 8
    tp2.Stop () // Stop sub task
    wait 3
    tp.Stop () // Stop whole task
    printfn "Op took %ims." (tp.Elapsed.TotalMilliseconds |> int64) // Report timings

open System.Net
open System.Xml.Linq

let linkCountOnPage (url : string) =
    use dp = new DisposingProfiler "Analyzing site"
    use client = new WebClient()
    let html = 
        use __ = dp.StartSubtask "Downloading"
        client.DownloadString url
    let result =
        use __ = dp.StartSubtask "Parsing HTML"
        let doc = XDocument.Parse html
        let rec getATag (elements : XElement seq) =
            elements
            |> Seq.filter (fun c -> c.Name.LocalName = "a")
            |> Seq.append (
                elements 
                |> Seq.filter (fun c -> c.Name.LocalName <> "a") 
                |> Seq.collect (fun c -> c.Descendants () |> getATag))
        doc.Root.Descendants () |> getATag |> Seq.length
    result 


// Preparing listeners and observers

let tObs = new ObservableTaskListener ()
let asd = tObs.Subscribe (TextCodec.print)
let qwe = tObs.Subscribe (JsonCodec.print)
let file = System.IO.File.OpenWrite(@"B:\FsProfiler.log")
let writer = new System.IO.StreamWriter(file)
let outputToFile = tObs.Subscribe (JsonCodec.fprint writer)

// Starting computations

DP ()
DP ()
WP ()
TP ()
linkCountOnPage "https://bing.com"

// Dispose listeners and observers

asd.Dispose ()
qwe.Dispose ()
outputToFile.Dispose ()
writer.Dispose ()
file.Dispose ()
tObs.Dispose ()


// Using Codes to read serialized events

let json = """{"Id":"4e52e052-0e47-4d34-8bc2-a62ccdd41e57","Name":"DP Level 0","SubTasks":[{"Id":"3a378205-3a77-49fb-a901-6c39bd00bf19","Name":"DP Level 1","SubTasks":[{"Id":"b012a411-414d-4788-b30c-807ab9bd201d","Name":"DP Level 2","SubTasks":[],"DurationInMilliseconds":5}],"DurationInMilliseconds":12}],"DurationInMilliseconds":34}
{"Id":"cf7fc3fd-7a79-4ad8-8ad3-43da1be42a70","Name":"DP Level 0","SubTasks":[{"Id":"78f587e3-9b52-490e-8797-6ee5b90310cf","Name":"DP Level 1","SubTasks":[{"Id":"5f3db898-b3a3-4a1d-8fda-d3fe391e6701","Name":"DP Level 2","SubTasks":[],"DurationInMilliseconds":4}],"DurationInMilliseconds":10}],"DurationInMilliseconds":30}
{"Id":"b7845403-8195-4eee-81c5-23b7f1411a34","Name":"Test","SubTasks":[],"DurationInMilliseconds":9}
{"Id":"8ee8aabc-47ea-49ee-b18e-c066acb1707c","Name":"TP 1","SubTasks":[{"Id":"9d8995d0-45f5-421d-b178-0d03dd9f8e10","Name":"TP 2","SubTasks":[],"DurationInMilliseconds":9}],"DurationInMilliseconds":26}
"""

json.Split([| Environment.NewLine; "\n" |], StringSplitOptions.RemoveEmptyEntries)
|> Array.map (JsonCodec.sscan >> TextCodec.sprint)
