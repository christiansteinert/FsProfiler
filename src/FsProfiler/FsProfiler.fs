namespace FsProfiler

open System
open System.Diagnostics
open System.Diagnostics.Tracing

type Task =
    {
        Id : Guid
        Name : string
        SubTasks : Task list
        DurationInMilliseconds : int64
    }

[<EventSource(Guid = "25cb5edc-07fc-57ae-8325-0f742b7c59f8")>]
type FsProfilerEvents private () =
    inherit EventSource ()

    static let log = new FsProfilerEvents ()

    static member Log = log

    [<Event(1, Message = "Task {0} started.", Level = EventLevel.LogAlways)>]
    member this.TaskStart(taskName : string, taskId : Guid) =
        #if DEBUG
        printfn "Task %s started." taskName
        #endif
        this.WriteEvent(1, taskName, taskId)

    [<Event(2, Message = "Subtask {0} of task {1} started.", Level = EventLevel.LogAlways)>]
    member this.SubtaskStart(taskName : string, taskId : Guid, parentTaskId : Guid) =
        #if DEBUG
        printfn "Subtask %s of task %O started." taskName taskId
        #endif
        this.WriteEvent(2, taskName, taskId, parentTaskId)

    [<Event(3, Message = "Task {0} took {1} ms.", Level = EventLevel.Informational)>]
    member this.TaskStop(taskName : string, milliseconds : int64, taskId : Guid) =
        #if DEBUG
        printfn "Task %s took %i ms." taskName milliseconds
        #endif
        this.WriteEvent(3, taskName, milliseconds, taskId)

    [<Event(4, Message = "Subtask {0} of task {3} took {1} ms.", Level = EventLevel.Informational)>]
    member this.SubtaskStop(taskName : string, milliseconds : int64, taskId : Guid, parentTaskId : Guid) =
        #if DEBUG
        printfn "Subtask %s of task %O took %i ms." taskName parentTaskId milliseconds
        #endif
        this.WriteEvent(4, taskName, milliseconds, taskId, parentTaskId)

    interface IDisposable with
        member __.Dispose () = log.Dispose ()

[<AbstractClass>]
type Profiler internal (taskName, parentTaskId : Guid option)  =
    let watch = Stopwatch ()
    let taskId = Guid.NewGuid ()

    member __.EventName = taskName
    member __.TaskId = taskId
    member internal __.Watch = watch
    
    member internal __.Start () = watch.Start ()
    member internal __.Stop () = watch.Stop ()

type DisposingProfiler private (taskName, parent) as this =
    inherit Profiler (taskName, parent)

    do
        match parent with
        | Some c -> FsProfilerEvents.Log.SubtaskStart(taskName, this.TaskId, c)
        | None -> FsProfilerEvents.Log.TaskStart(taskName, this.TaskId)
        this.Start ()

    new taskName = new DisposingProfiler (taskName, None)

    member this.StartSubtask taskName =
        new DisposingProfiler(taskName, this.TaskId |> Some)

    member this.Dispose () =
        this.Stop ()
        match parent with
        | Some c -> FsProfilerEvents.Log.SubtaskStop(taskName, this.Watch.ElapsedMilliseconds, this.TaskId, c)
        | None -> FsProfilerEvents.Log.TaskStop(taskName, this.Watch.ElapsedMilliseconds, this.TaskId)

    interface IDisposable with
        member this.Dispose () =
            this.Dispose ()

type TransientProfiler private (taskName, parent, suppressEvents) as this =
    inherit Profiler (taskName, parent)

    let mutable hasBeenStarted = false
    let mutable hasBeenStopped = false

    do
        this.Start ()

    new (taskName) = TransientProfiler (taskName, None, false)
    new (taskName, suppressEvents) = TransientProfiler (taskName, None, suppressEvents)


    member internal this.Start () = 
        if hasBeenStarted then
            raise (NotSupportedException("Profiler has been started before!"))
        else
            hasBeenStarted <- true
            if not suppressEvents then
                match parent with
                | Some c -> FsProfilerEvents.Log.SubtaskStart(taskName, this.TaskId, c)
                | None -> FsProfilerEvents.Log.TaskStart(taskName, this.TaskId)
            base.Start ()
    
    member this.Stop () = 
        if hasBeenStopped then
            raise (NotSupportedException("Profiler has been stopped before!"))
        elif not hasBeenStarted then
            raise (NotSupportedException("Profiler has not been started and cannot be stopped!"))
        else
            hasBeenStopped <- true
            base.Stop ()
            if not suppressEvents then
                match parent with
                | Some c -> FsProfilerEvents.Log.SubtaskStop(taskName, this.Watch.ElapsedMilliseconds, this.TaskId, c)
                | None -> FsProfilerEvents.Log.TaskStop(taskName, this.Watch.ElapsedMilliseconds, this.TaskId)

    member this.GetSubProfiler taskName =
        TransientProfiler(taskName, this.TaskId |> Some, false)

    member this.GetSubProfiler (taskName, suppressEvents) =
        TransientProfiler(taskName, this.TaskId |> Some, suppressEvents)

    member __.Elapsed = base.Watch.Elapsed

type WrappingProfiler<'a> private (taskName, f : unit -> 'a) =

    let taskId = Guid.NewGuid ()

    member private __.Run () =
        FsProfilerEvents.Log.TaskStart(taskName, taskId)
        let watch = Stopwatch.StartNew ()
        let result = f ()
        watch.Stop ()
        FsProfilerEvents.Log.TaskStop(taskName, watch.ElapsedMilliseconds, taskId)
        result

    static member Profile taskName f =
        let profiler = WrappingProfiler(taskName, f)
        profiler.Run ()
