﻿module FsProfiler.Codecs

open System
open System.IO
open System.Text

module TextCodec =

    /// Serializes a task to text and returns the string.
    let sprint task = 
        let builder = StringBuilder ()
        let rec printTask level task =
            Printf.bprintf builder "%*s%s\n" (level * 4) "" task.Name
            task.SubTasks |> List.iter (printTask (level + 1))
            Printf.bprintf builder "%*s--- %ims\n" (level * 4) "" task.DurationInMilliseconds
        task |> printTask 0
        builder.ToString ()

    /// Serializes a task to text and prints it to the console.
    let print task = 
        task |> sprint |> printf "%s" 

    /// Serializes a task to text appends it to a StringBuilder
    let bprint builder task =
        task |> sprint |> Printf.bprintf builder "%s"

    /// Serializes a task to text and prints it to the console's error stream.
    let eprint task =
        task |> sprint |> Printf.eprintf "%s"

    /// Serializes a task to text and appends it to a TextWriter's stream.
    let fprint writer task =
        task |> sprint |> Printf.fprintf writer "%s"
    
    /// Serializes a task to text appends it UTF-8 encoded to a stream.
    let rprint (stream : Stream) task =
        let text = task |> sprint
        let bytes =
            text + Environment.NewLine
            |> Encoding.UTF8.GetBytes
        
        stream.Write(bytes, 0, bytes.Length) 


module JsonCodec =

    open Newtonsoft.Json

    /// Serializes a task to Json  and returns the string.
    let sprint (task : Task) = 
        task |> JsonConvert.SerializeObject
    
    /// Deserializes the given Json to a task.
    let sscan text = 
        JsonConvert.DeserializeObject<Task> text

    /// Serializes a task to Json and prints it to the console.
    let print task = 
        task |> sprint |> printfn "%s" 

    /// Reads one line from the console and deserializes the read Json to a Task.
    let scan () =
        System.Console.ReadLine () |> sscan

    /// Serializes a task to Json and appends it to the StringBuilder.
    let bprint builder task =
        task |> sprint |> Printf.bprintf builder "%s\n"

    /// Serializes a task to Json and prints it to the console's error stream.
    let eprint task =
        task |> sprint |> Printf.eprintfn "%s"

    /// Serializes a task to Json and appends it to a TextWriter's stream.
    let fprint writer task =
        task |> sprint |> Printf.fprintfn writer "%s"

    /// Reads one line from a TextReader and deserializes the read Json to a Task.
    let fscan (reader : TextReader) =
        reader.ReadLine () |> sscan

    /// Serializes a task to Json and appends it UTF-8 encoded to a stream.
    let rprint (stream : Stream) task =
        let text = task |> sprint
        let bytes =
            text + Environment.NewLine
            |> Encoding.UTF8.GetBytes
        
        stream.Write(bytes, 0, bytes.Length)
