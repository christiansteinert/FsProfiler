// --------------------------------------------------------------------------------------
// FAKE build script
// --------------------------------------------------------------------------------------

#r "./packages/build/FAKE/tools/FakeLib.dll"

open Fake
open Fake.AssemblyInfoFile

// --------------------------------------------------------------------------------------
// Build variables
// --------------------------------------------------------------------------------------

System.Environment.CurrentDirectory <- __SOURCE_DIRECTORY__
let release = ReleaseNotesHelper.LoadReleaseNotes "CHANGELOG"
let mutable effectiveAssemblyVersion = ""

// --------------------------------------------------------------------------------------
// Targets
// --------------------------------------------------------------------------------------

Target "Clean" (fun _ -> DotNetCli.RunCommand id "clean")

Target "AssemblyInfo" (fun _ ->

    let getAssemblyInfoAttributes projectName =
        [ Attribute.Title (projectName)
          Attribute.Product "Fracer" ]

    let getProjectDetails projectPath =
        let projectName = System.IO.Path.GetFileNameWithoutExtension(projectPath)
        ( projectPath,
          projectName,
          System.IO.Path.GetDirectoryName(projectPath),
          (getAssemblyInfoAttributes projectName))

    !! "**/*.??proj"
    |> Seq.map getProjectDetails
    |> Seq.iter (fun (projFileName, projectName, folderName, attributes) ->
        match projFileName with
        | Fsproj -> 
            let asmInfoFile = folderName </> "AssemblyInfo.fs"
            let version = 
                let notesVersion = release.AssemblyVersion |> SemVerHelper.parse
                match GetAttribute "AssemblyVersion" asmInfoFile with
                | Some old -> 
                    let old = old.Value |> SemVerHelper.parse
                    if old.Major = notesVersion.Major && old.Minor = notesVersion.Minor then
                        { old with Patch = old.Patch + 1 }
                    else
                        notesVersion
                | None -> notesVersion
                |> (fun c -> Attribute.Version (c.ToString ()))

            let fileVersion = 
                let notesVersion = release.AssemblyVersion |> SemVerHelper.parse
                match GetAttribute "AssemblyFileVersion" asmInfoFile with
                | Some old -> 
                    let old = old.Value |> SemVerHelper.parse
                    if old.Major = notesVersion.Major && old.Minor = notesVersion.Minor then
                        { old with Patch = old.Patch + 1 }
                    else
                        notesVersion
                | None -> notesVersion
                |> (fun c -> Attribute.FileVersion (c.ToString ()))
            effectiveAssemblyVersion <- version.Value
            CreateFSharpAssemblyInfo asmInfoFile (version :: fileVersion :: attributes)
        | _ -> ()
        )
)

Target "Build" (fun _ -> DotNetCli.Build id)

Target "Pack" (fun _ -> DotNetCli.Pack (fun c -> 
    { 
        c with 
            AdditionalArgs = 
                [ 
                    sprintf "/p:PackageReleaseNotes=\"%s\"" (release.Notes |> List.reduce (+))
                    "/p:PackageVersion=" + effectiveAssemblyVersion 
                    "--no-build"
                ]
            OutputPath = "../../temp"
    }))

Target "Push" (fun _ -> Paket.Push (fun c -> 
    { 
        c with 
            WorkingDir = "./temp"
            PublishUrl = "http://proget.de.kworld.kpmg.com/nuget/KPMG.DE.KAIL.NUGET/"; ApiKey = "[APIKEY]" 
    }))

// --------------------------------------------------------------------------------------
// Build order
// --------------------------------------------------------------------------------------

"Clean"
    ==> "AssemblyInfo"
    ==> "Build"
    ==> "Pack"
    ==> "Push"

RunTargetOrDefault "Build"
