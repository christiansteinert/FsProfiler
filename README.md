# FsProfiler (needs a fancy name (Facer (F# Tracer)))

� let's you track the execution times of definable pieces of code.

## Recording Timings

### Listening

Set up a listener for events. 

``` F#
open FsProfiler.Listeners

use tObs = new ObservableTaskListener ()
```

This listener implements `IObservable<Task>`.
You can easily observe completed tasks by subscribing to that listener.
The easyest way is to just use one of the task formatters (see below).

```F#
open FsProfiler.Output

let asd = tObs.Subscribe (TaskCodec.print)
```

### Disposing Profiler

The `DisposingProfiler` tracks the time taken between its instanciation and disposition.
It is super easy to integrate and requires no manual "stopping".

```F#
open System.Net
open System.Xml.Linq

let linkCountOnPage (url : string) =
    use dp = new DisposingProfiler "Analyzing site"
    use client = new WebClient()
    let html = 
        use __ = dp.StartSubtask "Downloading"
        client.DownloadString url
    let result =
        use __ = dp.StartSubtask "Parsing HTML"
        let doc = XDocument.Parse html
        let rec getATag (elements : XElement seq) =
            elements
            |> Seq.filter (fun c -> c.Name.LocalName = "a")
            |> Seq.append (
                elements 
                |> Seq.filter (fun c -> c.Name.LocalName <> "a") 
                |> Seq.collect (fun c -> c.Descendants () |> getATag))
        doc.Root.Descendants () |> getATag |> Seq.length
    result
```

Execute the code

```F#
linkCountOnPage "https://bing.com"
```

As we subscribed to completed events with an printer that just outputs to the console we get the following output:

```F#
Analyzing site
    Downloading
    --- 230ms
    Parsing HTML
    --- 460ms
--- 690ms
```

### Transient Profiler

The `TransientProfiler` alows for more control over the end of a task and can be consumed later for immediate timing output.

```F#
let TP () =
    wait 2 // Prepare stuff
    let tp = TransientProfiler "TP 1" // Start the profiler
    wait 10
    let tp' = let tp = tp.GetSubProfiler "TP 2" // Get and start a sub profiler
    wait 8
    tp'.Stop () // Stop sub task
    wait 3
    tp.Stop () // Stop whole task
    printfn "Op took %ims." (tp.Elapsed.TotalMilliseconds |> int64) // Report timings
```

The subtask timings will have been reported via ETW events.
If you explicitly do not need or want the ETW events, because you read the Profiler directly, you can opt out of sending ETW Events:

```F#
let tp = TransientProfiler("Task 1", true)
```

## Codecs

FsProfiler comes with some codecs. A codecs is a module the serializes and deserializes `Task` instances.
The modules try to redproduce the F# Core's `Printf` module and add `scan` functions for input.

### TextCodec

The `FsProfiler.Codecs.TextCodec` provides formated text output to

- Console (`print`)
- `StringBuilder` (`brint`)
- Console's Error Stream (`eprint`)
- `TextWriter` (`fprint`) or
- as a plain `string` instance (`sprint`)

It prints the name of the task, its subtasks (indented by four spaces for each level), and then the time taken in milliseconds.
As the text output misses information from the task, such as the Id, this codec does not support any input. There are formats better suited for input than plain text.

### JsonCodec

Providing the same functions as the `TextCodec` module, this module formates the output of one task as a one line json object string, like:

```json
{"Id":"45c20611-be5b-415d-8100-91a26245f7f3","Name":"My Task","SubTasks":[]}
```

Additionaly it provides `sscan` (reading a Json line from `stdin`), `scan` (`string -> Task`) and `fscan`, that reads one line from a `TextReader`.

---

Codecs can be composed into more complex things, like `JsonCodec.sscan >> TextCodec.sprint`, that takes a line of Json and formats it as the corresponding text representation.